#!/bin/bash
my_dir="$(dirname "$0")"
t_d=$(date +"%m/%d/%Y")
t_h=$(date +"%T")

if ! [ -d "$my_dir/users" ];then
	mkdir "$my_dir/users"
fi

echo new username? 
read -s name

if [ ${#name} -lt 8 ];then
	echo REGISTER: ERROR Username must be atleast 8 character 
	echo "${t_d} ${t_h} REGISTER: ERROR Username must be atleast 8 character" >> $my_dir/log.txt
elif ! [[ $name =~ [[:alnum:]] ]];then 
	echo REGISTER: ERROR Username must be alphanumerical
	echo "${t_d} ${t_h} REGISTER: ERROR Username must be alphanumerical" >> $my_dir/log.txt
elif ! [[ $name =~ [[:upper:]] ]];then
	echo REGISTER: ERROR Username must contain uppercase
	echo "${t_d} ${t_h} REGISTER: ERROR Username must contain uppercase" >> $my_dir/log.txt
elif ! [[ $name =~ [[:lower:]] ]];then
	echo REGISTER: ERROR Username must contain lowercase
	echo "${t_d} ${t_h} REGISTER: ERROR Username must contain lowercase" >> $my_dir/log.txt
elif grep -q "user: $name" "$my_dir/users/user.txt";then
	echo REGISTER: ERROR User already exists
	echo "${t_d} ${t_h} REGISTER: ERROR User already exists" >> $my_dir/log.txt
else 
		echo new password?
		read -s pass 
	if [ "$name" == "$pass" ];then
		echo REGISTER: ERROR Password must be different than username
		echo "${t_d} ${t_h} REGISTER: ERROR Password must be different than username" >> $my_dir/log.txt
	elif [	${#pass} -lt 8 ];then
		echo REGISTER: ERROR Password must be atleast 8 character
		echo "${t_d} ${t_h} REGISTER: ERROR Password must be atleast 8 character" >> $my_dir/log.txt
	elif ! [[ $pass =~ [[:alnum:]] ]];then 
		echo REGISTER: ERROR Password must be alphanumerical
		echo "${t_d} ${t_h} REGISTER: ERROR Password must be alphanumerical" >> $my_dir/log.txt
	elif ! [[ $pass =~ [[:upper:]] ]];then
		echo REGISTER: ERROR Password must contain uppercase
		echo "${t_d} ${t_h} REGISTER: ERROR Password must contain uppercase" >> $my_dir/log.txt
	elif ! [[ $pass =~ [[:lower:]] ]];then
		echo REGISTER: ERROR Password must contain lowercase
		echo "${t_d} ${t_h} REGISTER: ERROR Password must contain lowercase" >> $my_dir/log.txt
	else
		echo REGISTER: INFO User ${name} registered successfully
		echo "${t_d} ${t_h} REGISTER: INFO User ${name} registered successfully" >> $my_dir/log.txt
		echo "user: ${name} pass: ${pass}" >> $my_dir/users/user.txt
	fi
fi
