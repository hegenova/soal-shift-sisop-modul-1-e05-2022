# soal-shift-sisop-modul-1-E05-2022

praktikum sisop

## Soal 1
### main.sh
```
my_dir="$(dirname "$0")"
t_d=$(date +"%m/%d/%Y")
t_h=$(date +"%T")
t_d1=$(date +"%Y-%m-%d")
```
sebagai inisialisasi lokasi direktori beserta variabel yang menyimpan format tanggal dan waktu
<br /> <br /> 
```
echo enter your username
read -s name

if grep -q "user: $name" "$my_dir/users/user.txt";then
```
membaca input dan mencari jika input username ada di database user (user.txt)
<br /> <br />
```
echo enter your password
	read -s pass
	
	if grep -q "user: $name pass: $pass" "$my_dir/users/user.txt";then
```
membaca input dan mencari jika input username dan password sesuai dan ada di database user (user.txt)
<br /> <br />
```
echo LOGIN: INFO User $user logged in
		echo "${t_d} ${t_h} LOGIN: INFO User $user logged in" >> $my_dir/log.txt
		echo 'enter command ("dl N(number)" or "att")'
		read cmd
```
mengirimkan output login berhasil lalu menyimpannya ke log.txt
setelah itu mengirimkan output yang menanyakan pilihan user antara command dl N atau att dan membaca input setelahnya
<br /> <br />
```
	if [ "$cmd" == "att" ];then
			grep -c "LOGIN: " "$my_dir/log.txt"
```
jika user mengetik input att, maka program akan mengoutputkan jumlah percobaan login yang gagal dan berhasil
<br /> <br />
```
else 
			num=$(echo "$cmd" | sed 's/[^0-9]*//g')
			if [ -e $my_dir/"$t_d1"_$name.zip ];then
				unzip -P $pass $my_dir/"$t_d1"_$name.zip -d $my_dir/
				mkdir $my_dir/"$t_d1"_$name
				find $my_dir/$my_dir/"$t_d1"_$name -type f -print0 | xargs -0 mv -t $my_dir/"$t_d1"_$name
				last_file=$(ls -1 $my_dir/"$t_d1"_$name | tail -n 1)
				last_num=$(echo "$last_file" | sed 's/[^0-9]*//g')
				for((i=last_num+1; i<num+last_num+1; i++))
				do
				if [ $i -lt 10 ];then
					urut=0$i
				else
					urut=$i
				fi
				wget -nd -r -P $my_dir/"$t_d1"_$name / --output-document=$my_dir/"$t_d1"_$name/PIC_$urut.jpg https://loremflickr.com/320/240
				done
				zip --password $pass -r $my_dir/"$t_d1"_$name.zip $my_dir/"$t_d1"_$name
				base_dir=$(echo "$my_dir" | cut -d "/" -f2)
				rm -r $my_dir/$base_dir/
				rm -r $my_dir/"$t_d1"_$name/
			else
				if ! [ -d $my_dir/"$t_d1"_$name ];then
					mkdir $my_dir/"$t_d1"_$name
				fi
				for((i=1; i<num+1; i++))
				do
				if [ $i -lt 10 ];then
					urut=0$i
				else
					urut=$i
				fi
				wget -nd -r -P $my_dir/"$t_d1"_$name / --output-document=$my_dir/"$t_d1"_$name/PIC_$urut.jpg https://loremflickr.com/320/240
				done
				zip --password $pass -r $my_dir/"$t_d1"_$name.zip $my_dir/"$t_d1"_$name 
				rm -r $my_dir/"$t_d1"_$name/
			fi
		fi
```
jika user mengetikkan input dl, maka akan masuk ke else dan angka yang diinput tadi akan diekstrak ke variabel num. variabel num ini digunakan untuk melakukan loop sebanyak num yang dimana didalamnya terdapat **wget** untuk mendownload file gambar. terdapat dua kondisional yang dimana cara kerjanya mirip, kondisional pertama tereksekusi ketika file zip yang akan dibuat dari file yang didownload telah ada, sedangkan kondisional kedua tereksekusi ketika file zip yang akan dibuat dari file yang didownload belum ada

 maka kondisional pertama me-unzip lalu mengekstrak gambar dari folder yang telah diunzip, membuat folder baru yang disesuaikan dengan **YYYY-MM-DD_USERNAME** dan memindahkan gambar yang telah di ekstrak tersebut kedalam folder yang baru dibuat. urutan dari gambar diambil dari gambar paling akhir berdasarkan alphabetical order, diekstrak angkanya, lalu disesuaikan untuk dimasukkan ke loop. setelah semua gambar terdownload dan tertambahkan di folder baru, folder akan di zip lagi dengan menambahkan password sesuai dengan password user. lalu langkah terakhir adalah menghapus folder yang di unzip serta folder baru yang dibuat tadi supaya lebih rapih.

 kondisional kedua membuat folder baru yang disesuaikan dengan **YYYY-MM-DD_USERNAME** lalu mendownload gambar sebanyak num yang telah diinputkan user sebelumnya. gambar tersebut disimpan di folder yang baru kita buat lalu folder akan di zip dengan password yang sama dengan password login user. terakhir hapus folder baru yang dibuat supaya lebih rapih.
<br /> <br />
 ```
 else
		echo LOGIN: ERROR Failed login attempt on user $user
		echo "${t_d} ${t_h} LOGIN: ERROR Failed login attempt on user $user" >> $my_dir/log.txt
	fi
 ```
 kondisional ini tereksekusi ketika user salah memasukkan password(tidak cocok dengan yang ada pada user.txt). program akan mengoutputkan error lalu mengirimkannya kedalam log.txt
<br /> <br />
 ```
 else
	echo LOGIN: ERROR User not found
	echo "${t_d} ${t_h} LOGIN: ERROR User not found" >> $my_dir/log.txt
fi
 ```
kondisional ini tereksekusi ketika user memasukkan username yang tidak ada pada user.txt. program akan mengoutpukan error lalu mengirimkannya kedalam log.txt
<br /> <br /> <br /> <br />

### register.sh
```
my_dir="$(dirname "$0")"
t_d=$(date +"%m/%d/%Y")
t_h=$(date +"%T")
```
sebagai inisialisasi lokasi direktori beserta variabel yang menyimpan format tanggal dan waktu
<br /> <br />
```
if ! [ -d "$my_dir/users" ];then
	mkdir "$my_dir/users"
fi
```
membuat folder user jika folder user belum ada di direktori 
<br /> <br />
```
echo new username? 
read -s name
```
mengoutpukan pertanyaan pada user sebagai registrasi username baru. lalu username akan disimpan pada variabel name
<br /> <br />
```
if [ ${#name} -lt 8 ];then
	echo REGISTER: ERROR Username must be atleast 8 character 
	echo "${t_d} ${t_h} REGISTER: ERROR Username must be atleast 8 character" >> $my_dir/log.txt
elif ! [[ $name =~ [[:alnum:]] ]];then 
	echo REGISTER: ERROR Username must be alphanumerical
	echo "${t_d} ${t_h} REGISTER: ERROR Username must be alphanumerical" >> $my_dir/log.txt
elif ! [[ $name =~ [[:upper:]] ]];then
	echo REGISTER: ERROR Username must contain uppercase
	echo "${t_d} ${t_h} REGISTER: ERROR Username must contain uppercase" >> $my_dir/log.txt
elif ! [[ $name =~ [[:lower:]] ]];then
	echo REGISTER: ERROR Username must contain lowercase
	echo "${t_d} ${t_h} REGISTER: ERROR Username must contain lowercase" >> $my_dir/log.txt
elif grep -q "user: $name" "$my_dir/users/user.txt";then
	echo REGISTER: ERROR User already exists
	echo "${t_d} ${t_h} REGISTER: ERROR User already exists" >> $my_dir/log.txt
else 
		echo new password?
		read -s pass 
```
mengecek jika username yang diinputkan sesuai dengan kriteria. jika kurang dari 8 karakter maka akan mengoutputkan error lalu error akan dikirim ke log.txt. jika tidak memiliki alphanumeric(mengecek dengan menggunakan regex :alnum:) maka akan mengoutputkan error lalu error akan dikirim ke log.txt. jika tidak memiliki uppercase(mengecek dengan menggunakan regex :upper:) maka akan mengoutputkan error lalu error akan dikirim ke log.txt. jika tidak memiliki lowercase(mengecek dengan menggunakan regex :lower:) maka akan mengoutputkan error lalu error akan dikirim ke log.txt. jika username sudah ada di file user.txt maka akan mengoutputkan error lalu error akan dikirim ke log.txt

jika memenuhi semua syarat diatas maka akan lanjut ke else dimana program akan mengoutputkan pertanyaan pada user sebagai registrasi password dari username yang baru.
<br /> <br />
```
if [ "$name" == "$pass" ];then
		echo REGISTER: ERROR Password must be different than username
		echo "${t_d} ${t_h} REGISTER: ERROR Password must be different than username" >> $my_dir/log.txt
	elif [	${#pass} -lt 8 ];then
		echo REGISTER: ERROR Password must be atleast 8 character
		echo "${t_d} ${t_h} REGISTER: ERROR Password must be atleast 8 character" >> $my_dir/log.txt
	elif ! [[ $pass =~ [[:alnum:]] ]];then 
		echo REGISTER: ERROR Password must be alphanumerical
		echo "${t_d} ${t_h} REGISTER: ERROR Password must be alphanumerical" >> $my_dir/log.txt
	elif ! [[ $pass =~ [[:upper:]] ]];then
		echo REGISTER: ERROR Password must contain uppercase
		echo "${t_d} ${t_h} REGISTER: ERROR Password must contain uppercase" >> $my_dir/log.txt
	elif ! [[ $pass =~ [[:lower:]] ]];then
		echo REGISTER: ERROR Password must contain lowercase
		echo "${t_d} ${t_h} REGISTER: ERROR Password must contain lowercase" >> $my_dir/log.txt
	else
		echo REGISTER: INFO User ${name} registered successfully
		echo "${t_d} ${t_h} REGISTER: INFO User ${name} registered successfully" >> $my_dir/log.txt
		echo "user: ${name} pass: ${pass}" >> $my_dir/users/user.txt
	fi
```
mengecek jika password sesuai dengan kriteria. jika sama dengan username aka akan mengoutputkan error lalu error akan dikirim ke log.txt. jika kurang dari 8 karakter aka akan mengoutputkan error lalu error akan dikirim ke log.txt. jika tidak memiliki alphanumeric(mengecek dengan menggunakan regex :alnum:) maka akan mengoutputkan error lalu error akan dikirim ke log.txt. jika tidak memiliki uppercase(mengecek dengan menggunakan regex :upper:) maka akan mengoutputkan error lalu error akan dikirim ke log.txt. jika tidak memiliki lowercase(mengecek dengan menggunakan regex :lower:) maka akan mengoutputkan error lalu error akan dikirim ke log.txt.

jika memenuhi semua syarat diatas maka akan lanjut ke else dimana program akan mengoutputkan **REGISTER: INFO User ${name} registered successfully** lalu output akan dikirim ke log.txt. 

setelah itu program menyimpan username dan password yang telah memenuhi syarat tersebut menjadi satu line ke dalam users/user.txt


**Soal 3**

- Command
    - free -m
    - du -sh /home/{user}/ → du -sh /home/$USER/

## minute_log.sh

---

```bash
#!/bin/bash
tgl=$(date +"20%y%m%d%k%M%S")

m_tot=$(free -m | awk 'FNR == 2 {print}' | awk '{print $2}')
m_used=$(free -m | awk 'FNR == 2 {print}' | awk '{print $3}')
m_free=$(free -m | awk 'FNR == 2 {print}' | awk '{print $4}')
m_shared=$(free -m | awk 'FNR == 2 {print}' | awk '{print $5}')
m_buff=$(free -m | awk 'FNR == 2 {print}' | awk '{print $6}')
m_avlb=$(free -m | awk 'FNR == 2 {print}' | awk '{print $7}')

s_tot=$(free -m | awk 'FNR == 3 {print}' | awk '{print $2}')
s_used=$(free -m | awk 'FNR == 3 {print}' | awk '{print $3}')
s_free=$(free -m | awk 'FNR == 3 {print}' | awk '{print $4}')

pth_sz=$(du -sh /home/$USER/ | awk '{print $1}')
pth_pth=$(du -sh /home/$USER/ | awk '{print $2}')

otp="${m_tot},${m_used},${m_free},${m_shared},${m_buff},${m_avlb},${s_tot},${s_used},${s_free},${pth_pth},${pth_sz}"

echo ${otp} > /home/$USER/log/metrics_${tgl}.log

chmod 400 /home/$USER/log/metrics_${tgl}.log

# */1 * * * * /home/fathur45/Documents/VsCode/SISOP/Gitlab/soal-shift-sisop-modul-1-e05-2022/soal3/minute_log.sh
```

---

**Pengambilan Nama File**

```bash
#!/bin/bash
tgl=$(date +"20%y%m%d%k%M%S")
```

disini kita mengambil data sesuai permintaan soal yaitu

`%y` = tahun
`%m` = bulan
`%d` = hari 
`%k` = jam dalam format 24
`%M` = menit
`%S` = detik

Hasil di terminal sebagai berikut :

```jsx
20220304221734
```

---

**Ambil tiap attribute dari (free -m) dan (du -sh)**

```bash
m_tot=$(free -m | awk 'FNR == 2 {print}' | awk '{print $2}')
m_used=$(free -m | awk 'FNR == 2 {print}' | awk '{print $3}')
m_free=$(free -m | awk 'FNR == 2 {print}' | awk '{print $4}')
m_shared=$(free -m | awk 'FNR == 2 {print}' | awk '{print $5}')
m_buff=$(free -m | awk 'FNR == 2 {print}' | awk '{print $6}')
m_avlb=$(free -m | awk 'FNR == 2 {print}' | awk '{print $7}')

s_tot=$(free -m | awk 'FNR == 3 {print}' | awk '{print $2}')
s_used=$(free -m | awk 'FNR == 3 {print}' | awk '{print $3}')
s_free=$(free -m | awk 'FNR == 3 {print}' | awk '{print $4}')

pth_sz=$(du -sh /home/$USER/ | awk '{print $1}')
pth_pth=$(du -sh /home/$USER/ | awk '{print $2}')

otp="${m_tot},${m_used},${m_free},${m_shared},${m_buff},${m_avlb},${s_tot},${s_used},${s_free},${pth_pth},${pth_sz}"
```

Kode ini mengambil kolom dan baris dimana :

- Baris/line
    
    ```bash
    free -m | awk 'FNR == 2 {print}' # Print line ke dua
    
    # Output :
    # Mem:          15413        4136        7637         249        3640       10708
    ```
    
- Kolom
    
    ```bash
    free -m | awk '{print $2}' # Print kolom kedua
    
    # Output :
    # used
    # 15413
    # 2047
    ```
    

Semua attribute tersebut disimpan sesuai formal soal di dalam `otp`

```bash
otp="${m_tot},${m_used},${m_free},${m_shared},${m_buff},${m_avlb},${s_tot},${s_used},${s_free},${pth_pth},${pth_sz}"
```

---

**Membuat File Log dan Mengganti Permission**

```bash
echo ${otp} > /home/$USER/log/metrics_${tgl}.log

chmod 400 /home/$USER/log/metrics_${tgl}.log
```

- `echo >` memasukan isi dati `otp` kedalam file baru yaitu `/home/$USER/log/metrics_${tgl}.log`
- lalu kita ubah permission agar hanya bisa dibuka oleh user sekarang menggukana `chmod 400`

Isi dari file sebagai berikut :

```bash
15413,2688,9964,95,2761,12306,2047,0,2047,/home/fathur45/,5,7G
```

---

**Chronjob**

```bash
*/1 * * * * /home/fathur45/Documents/VsCode/SISOP/Gitlab/soal-shift-sisop-modul-1-e05-2022/soal3/minute_log.sh
```

Disini kita menjalankan file ini perjam dilihat dari `*/1 * * * *`

## aggregate_minutes_to_hourly_log.sh

---

```bash
#!/bin/bash
tlg_wkt=$(date +"20%y%m%d%k%M%S")

fl_all=$(ls /home/$USER/log/.)
mkdir /home/$USER/log/agg
mkdir /home/$USER/log/tmp

# get time rn
curr=$(date +"%k%M")
curr=$(expr $curr - 0)

let bfr=$curr-100

for f in $fl_all; do
    validator=$(echo $f | cut -c24-26)
    if [ "$validator" = "log" ] && [ "$validator" != "" ]
    then
        tmp=$(echo $f | cut -c17-20)
        tmp=$(expr $tmp - 0)
        
        # if bfr <= tmp <= curr
        if [ $bfr -le $tmp ] && [ $curr -ge $tmp ]
        then
            mem=$(cat /home/$USER/log/${f} | awk -F, '{print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12}')
            ln=$(cat /home/$USER/log/${f} | awk -F, '{print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10}')
            strg="1"

            x=$(echo $mem | awk '{print $12}')

            if [ $x = " " ]
            then
                strg=$(echo $mem | awk '{print $11}' | tr -s '[:blank:]' '.')
            else
                strg=$(echo $mem | awk '{print $11, $12}' | tr -s '[:blank:]' '.')
            fi

            STR=$strg
            SUB="M"
            if [[ "$STR" == *"$SUB"* ]]; then
                deleted=$(echo $STR | tr -d 'M')
                strg=$deleted
            else
                deleted=$(echo $STR | tr -d 'G')
                convert=$(bc <<< "$deleted * 1024")
                strg=${convert%\.*}
            fi

            output="$ln $strg" 
            echo $output >> /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log
        fi
    fi
done

max1=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $1}' | sort -rn | head -n 1)
max2=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $2}' | sort -rn | head -n 1)
max3=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $3}' | sort -rn | head -n 1)
max4=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $4}' | sort -rn | head -n 1)
max5=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $5}' | sort -rn | head -n 1)
max6=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $6}' | sort -rn | head -n 1)
max7=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $7}' | sort -rn | head -n 1)
max8=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $8}' | sort -rn | head -n 1)
max9=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $9}' | sort -rn | head -n 1)
max11=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $11}' | sort -rn | head -n 1)

min1=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $1}' | sort -n | head -n 1)
min2=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $2}' | sort -n | head -n 1)
min3=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $3}' | sort -n | head -n 1)
min4=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $4}' | sort -n | head -n 1)
min5=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $5}' | sort -n | head -n 1)
min6=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $6}' | sort -n | head -n 1)
min7=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $7}' | sort -n | head -n 1)
min8=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $8}' | sort -n | head -n 1)
min9=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $9}' | sort -n | head -n 1)
min11=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $11}' | sort -n | head -n 1)

avg1=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $1 } END { print total/NR }')
avg2=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $2 } END { print total/NR }')
avg3=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $3 } END { print total/NR }')
avg4=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $4 } END { print total/NR }')
avg5=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $5 } END { print total/NR }')
avg6=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $6 } END { print total/NR }')
avg7=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $7 } END { print total/NR }')
avg8=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $8 } END { print total/NR }')
avg9=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $9 } END { print total/NR }')
avg11=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $11 } END { print total/NR }')

pth=$(du -sh /home/$USER/ | awk '{print $2}')

mx="Max: ${max1},${max2},${max3},${max4},${max5},${max6},${max7},${max8},${max9},${pth},${max11}M"
mn="Min: ${min1},${min2},${min3},${min4},${min5},${min6},${min7},${min8},${min9},${pth},${min11}M"
avg="Avg: ${avg1},${avg2},${avg3},${avg4},${avg5},${avg6},${avg7},${avg8},${avg9},${pth},${avg11}M"

echo ${mx} >> /home/$USER/log/agg/metrics_agg_${tlg_wkt}.log
echo ${mn} >> /home/$USER/log/agg/metrics_agg_${tlg_wkt}.log
echo ${avg} >> /home/$USER/log/agg/metrics_agg_${tlg_wkt}.log

chmod 400 /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log
chmod 400 /home/$USER/log/agg/metrics_agg_${tlg_wkt}.log

# * */1 * * * /home/fathur45/Documents/VsCode/SISOP/Gitlab/soal-shift-sisop-modul-1-e05-2022/soal3/aggregate_minutes_to_hourly_log.sh
```

---

**Ambil Waktu Sekarang, Nama File yang Akan Dibuat, dan List dari Semua File dalam Log**

```bash
#!/bin/bash
tlg_wkt=$(date +"20%y%m%d%k%M%S")

fl_all=$(ls /home/$USER/log/.)
mkdir /home/$USER/log/agg
mkdir /home/$USER/log/tmp

# get time rn
curr=$(date +"%k%M")
curr=$(expr $curr - 0)

let bfr=$curr-100
```

- `tlg_wkt` disini sama dengan soal diatas yaitu mengambil nama file aggregate
- `fl_all` mengambil list dari semua file yang berada di `/home/$USER/log/.`
- `mkdir` disini kita membuat folder untuk
    - Tempat meletakan file kalkulasi (`/tmp`)
    - Tempat Meletakan log per jam (`/agg`)

---

**Loop Seluruh File dalam log**

```bash
for f in $fl_all; do
    validator=$(echo $f | cut -c24-26)

    if [ "$validator" = "log" ] && [ "$validator" != "" ]
    then
        tmp=$(echo $f | cut -c17-20)
        tmp=$(expr $tmp - 0)
        
        # if bfr <= tmp <= curr
        if [ $bfr -le $tmp ] && [ $curr -ge $tmp ]
        then
            mem=$(cat /home/$USER/log/${f} | awk -F, '{print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12}')
            ln=$(cat /home/$USER/log/${f} | awk -F, '{print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10}')
            
						strg="1"
            x=$(echo $mem | awk '{print $12}')

            if [ $x = " " ]
            then
                strg=$(echo $mem | awk '{print $11}' | tr -s '[:blank:]' '.')
            else
                strg=$(echo $mem | awk '{print $11, $12}' | tr -s '[:blank:]' '.')
            fi

            STR=$strg
            SUB="M"
            if [[ "$STR" == *"$SUB"* ]]; then
                deleted=$(echo $STR | tr -d 'M')
                strg=$deleted
            else
                deleted=$(echo $STR | tr -d 'G')
                convert=$(bc <<< "$deleted * 1024")
                strg=${convert%\.*}
            fi

            output="$ln $strg" 
            echo $output >> /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log
        fi
    fi
done
```

- `validator
echo $f | cut -c24-26` , ini akan mengambil nama file sekarang (`$f`) lalu mengambil index ke 24 sampai 26. Alasannya adalah agar `tmp` dan `agg` tidak kita masukan kedalam file kalkulasi
    
    ```bash
    metrics_20220303234824.log # 24 -26 disini adalah "log"
    ```
    

- `if [ "$validator" = "log" ] && [ "$validator" != "" ]`
Kita memvalidasikan jika index 24 - 26 file tersebut “log” dan tidak NULL (`””`)

- File Kalkulasi
    
    File kalkulasi disini berisi baris-baris file yang di run selama satu jam, jika contohkan akan berisi seperti berikut
    
    ```bash
    15413 6066 6011 397 3335 8627 2047 0 2047 /home/fathur45/ 5632
    15413 2688 9964 95 2761 12306 2047 0 2047 /home/fathur45/ 5836
    ```
    
    Tujuannya disini agar mudah untuk mencari max, min, dan average tiap kolom
    
    ---
    
    ```bash
    mem=$(cat /home/$USER/log/${f} | awk -F, '{print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12}')
    ln=$(cat /home/$USER/log/${f} | awk -F, '{print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10}')
    strg="1"
    x=$(echo $mem | awk '{print $12}')
    ```
    
    `mem` : 
    `cat` akan mengambil isi file dari file log `$f` lalu `awk` akan menghapus `,` menggunakan `-F,` , disini nantinya kita akan mengambil keseluruhan File
    
    ```bash
    # Dari
    15413,2688,9964,95,2761,12306,2047,0,2047,/home/fathur45/,5,7G
    
    # Menjadi
    15413 2688 9964 95 2761 12306 2047 0 2047 /home/fathur45/ 5 7G
    ```
    
    `ln` :
    `ln` sama seperti `mem` namun kita tidang mengambil directory `/home/fathur45` dan besar file `5 5G`
    
    ```bash
    # Dari
    15413,2688,9964,95,2761,12306,2047,0,2047
    
    # Menjadi
    15413 2688 9964 95 2761 12306 2047 0 2047
    ```
    
    `strg` :
    
    Hanya sebagai placeholder, nantinya akan dipakai untuk mengkalkulasi size directory karena format sekarang adalah `5 7G` dimana seharusnya megabyte
    
    `x` :
    
    `x` mengambil `7G` dari `5 7G` dikarenakan jika dia gigabyte maka akan ada koma yang karena dihapus semua koma kita harus melakukan kalkulasi dan penempatan variable khusus
    
    ---
    
    **Masalah Size Directory yang Format Gigabyte**
    
    ```bash
    if [ $x = " " ]
    then
    		strg=$(echo $mem | awk '{print $11}' | tr -s '[:blank:]' '.')
    else
        strg=$(echo $mem | awk '{print $11, $12}' | tr -s '[:blank:]' '.')
    fi
    ```
    
    - `if [ $x = " " ]`
        - Jika `x` kosong (berarti format dalam berkoma, baik gigabyte atau megabyte), maka kita masukan `$11` yaitu contoh `57M` kedalam `strg`
        - Jika tidak kosong maka itu tidak berkoma disini kita tambag `.` agar bisa diproses dalam bentuk float contoh `5 7G` menjadi `5.7G`
    
    - Konversi Gigabyte ke Megabyte
    
    ```bash
    STR=$strg
    SUB="M"
    if [[ "$STR" == *"$SUB"* ]]; then
       deleted=$(echo $STR | tr -d 'M')
       strg=$deleted
    else
       deleted=$(echo $STR | tr -d 'G')
    	 convert=$(bc <<< "$deleted * 1024")
       strg=${convert%\.*}
    fi
    ```
    
    - `STR` kita samakan `strg` untuk komperasi, dan `SUB` disini adalah huruf M
    - `if [[ "$STR" == *"$SUB"* ]]`
        - Jika ada huruf M dalam `STR` maka itu adalah megabyte, tidak kita kalkulasi lalu kita hapus M agar bisa di min max dan average
        - Jika tidak huruf M berarti di gigabyte dimana disini kita hilangkan huruf G lalu kalikan float tersebut menggunakan `bc` dan menghilangkan semua titik serta angka di belakang menggunakan `convert%\.*`
            
            ```bash
            # Dari 5.57 menjadi 5632
            ```
            
    
    - Membuat hasil kalkulasi kedalam file
    
    ```bash
    # Gabung 15413 2688 9964 95 2761 12306 2047 0 2047 dengan 5632
    output="$ln $strg"
    
    # Membuat file dalam folder tmp
    echo $output >> /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log
    ```
    

---

**Max, Min, dan Average**

- Max
    
    ```bash
    max1=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $1}' | sort -rn | head -n 1)
    max2=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $2}' | sort -rn | head -n 1)
    max3=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $3}' | sort -rn | head -n 1)
    max4=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $4}' | sort -rn | head -n 1)
    max5=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $5}' | sort -rn | head -n 1)
    max6=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $6}' | sort -rn | head -n 1)
    max7=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $7}' | sort -rn | head -n 1)
    max8=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $8}' | sort -rn | head -n 1)
    max9=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $9}' | sort -rn | head -n 1)
    max11=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $11}' | sort -rn | head -n 1)
    ```
    
    Code utama disini adalah
    
    ```bash
    cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $1}' | sort -rn | head -n 1
    ```
    
    disini kita mengambil file tmp pada waktu sekarang, lalu `awk` mengambil tiap kolom dan mensort berdasar desc menggunakan `sort -rn` lalu kita ambil paling atas `head -n 1`
    
- Min
    
    ```bash
    min1=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $1}' | sort -n | head -n 1)
    min2=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $2}' | sort -n | head -n 1)
    min3=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $3}' | sort -n | head -n 1)
    min4=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $4}' | sort -n | head -n 1)
    min5=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $5}' | sort -n | head -n 1)
    min6=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $6}' | sort -n | head -n 1)
    min7=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $7}' | sort -n | head -n 1)
    min8=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $8}' | sort -n | head -n 1)
    min9=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $9}' | sort -n | head -n 1)
    min11=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $11}' | sort -n | head -n 1)cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $1}' | sort -n | head -n 1
    ```
    
    Code utama disini adalah
    
    ```bash
    cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $1}' | sort -rn | head -n 1
    ```
    
    disini kita mengambil file tmp pada waktu sekarang, lalu `awk` mengambil tiap kolom dan mensort berdasar ascend menggunakan `sort -n` lalu kita ambil paling atas `head -n 1`
    

- Average
    
    ```bash
    avg1=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $1 } END { print total/NR }')
    avg2=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $2 } END { print total/NR }')
    avg3=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $3 } END { print total/NR }')
    avg4=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $4 } END { print total/NR }')
    avg5=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $5 } END { print total/NR }')
    avg6=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $6 } END { print total/NR }')
    avg7=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $7 } END { print total/NR }')
    avg8=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $8 } END { print total/NR }')
    avg9=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $9 } END { print total/NR }')
    avg11=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $11 } END { print total/NR }')
    ```
    
    Code utama disini adalah
    
    ```bash
    cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $1 } END { print total/NR }'
    ```
    
    disini kita mengambil file tmp pada waktu sekarang, lalu `awk` mengambil tiap kolom lalu menggunakan `total` untuk menambahkan isi dari tiap kolom kedalam `total` lalu print `total` dibagi jumlah line
    

---

**Masuk Ke Dalam Folder agg**

```bash
pth=$(du -sh /home/$USER/ | awk '{print $2}')

mx="Max: ${max1},${max2},${max3},${max4},${max5},${max6},${max7},${max8},${max9},${pth},${max11}M"
mn="Min: ${min1},${min2},${min3},${min4},${min5},${min6},${min7},${min8},${min9},${pth},${min11}M"
avg="Avg: ${avg1},${avg2},${avg3},${avg4},${avg5},${avg6},${avg7},${avg8},${avg9},${pth},${avg11}M"

echo ${mx} >> /home/$USER/log/agg/metrics_agg_${tlg_wkt}.log
echo ${mn} >> /home/$USER/log/agg/metrics_agg_${tlg_wkt}.log
echo ${avg} >> /home/$USER/log/agg/metrics_agg_${tlg_wkt}.log
```

Disini kita gabung semua max min dan avg menjadi variable lalu kita masukan kedalam file baru

---

**Permission**

```bash
chmod 400 /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log
chmod 400 /home/$USER/log/agg/metrics_agg_${tlg_wkt}.log
```

Lock kedua file untuk user only

Hasilnya sebagai berikut

tmp

```bash
15413 8530 2611 468 4272 6093 2047 0 2047 /home/fathur45/ 5632
15413 4242 7517 134 3653 10719 2047 0 2047 /home/fathur45/ 5836
15413 4242 7518 134 3653 10719 2047 0 2047 /home/fathur45/ 5836
15413 4244 7519 130 3649 10721 2047 0 2047 /home/fathur45/ 5836
15413 4243 7520 130 3649 10722 2047 0 2047 /home/fathur45/ 5836
```

agg

```bash
Max: 15413,8530,7520,468,4272,10722,2047,0,2047,/home/fathur45/,5836M
Min: 15413,4242,2611,130,3649,6093,2047,0,2047,/home/fathur45/,5632M
Avg: 15413,5100,2,6537,199,2,3775,2,9794,8,2047,0,2047,/home/fathur45/,5795,2M
```

---

**Chronjob**

```bash
* */1 * * * /home/fathur45/Documents/VsCode/SISOP/Gitlab/soal-shift-sisop-modul-1-e05-2022/soal3/aggregate_minutes_to_hourly_log.sh
```

Cronjob ini menjalankan file perjam