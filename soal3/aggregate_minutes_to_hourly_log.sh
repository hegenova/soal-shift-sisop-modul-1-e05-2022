#!/bin/bash
tlg_wkt=$(date +"20%y%m%d%k%M%S")

fl_all=$(ls /home/$USER/log/.)
mkdir /home/$USER/log/agg
mkdir /home/$USER/log/tmp

# get time rn
curr=$(date +"%k%M")
curr=$(expr $curr - 0)

let bfr=$curr-100

for f in $fl_all; do
    validator=$(echo $f | cut -c24-26)
    if [ "$validator" = "log" ] && [ "$validator" != "" ]
    then
        tmp=$(echo $f | cut -c17-20)
        tmp=$(expr $tmp - 0)
        
        # if bfr <= tmp <= curr
        if [ $bfr -le $tmp ] && [ $curr -ge $tmp ]
        then
            mem=$(cat /home/$USER/log/${f} | awk -F, '{print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12}')
            ln=$(cat /home/$USER/log/${f} | awk -F, '{print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10}')
            strg="1"

            x=$(echo $mem | awk '{print $12}')

            if [ $x = " " ]
            then
                strg=$(echo $mem | awk '{print $11}' | tr -s '[:blank:]' '.')
            else
                strg=$(echo $mem | awk '{print $11, $12}' | tr -s '[:blank:]' '.')
            fi

            STR=$strg
            SUB="M"
            if [[ "$STR" == *"$SUB"* ]]; then
                deleted=$(echo $STR | tr -d 'M')
                strg=$deleted
            else
                deleted=$(echo $STR | tr -d 'G')
                convert=$(bc <<< "$deleted * 1024")
                strg=${convert%\.*}
            fi

            output="$ln $strg" 
            echo $output >> /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log
        fi
    fi
done

max1=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $1}' | sort -rn | head -n 1)
max2=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $2}' | sort -rn | head -n 1)
max3=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $3}' | sort -rn | head -n 1)
max4=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $4}' | sort -rn | head -n 1)
max5=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $5}' | sort -rn | head -n 1)
max6=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $6}' | sort -rn | head -n 1)
max7=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $7}' | sort -rn | head -n 1)
max8=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $8}' | sort -rn | head -n 1)
max9=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $9}' | sort -rn | head -n 1)
max11=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $11}' | sort -rn | head -n 1)

min1=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $1}' | sort -n | head -n 1)
min2=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $2}' | sort -n | head -n 1)
min3=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $3}' | sort -n | head -n 1)
min4=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $4}' | sort -n | head -n 1)
min5=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $5}' | sort -n | head -n 1)
min6=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $6}' | sort -n | head -n 1)
min7=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $7}' | sort -n | head -n 1)
min8=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $8}' | sort -n | head -n 1)
min9=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $9}' | sort -n | head -n 1)
min11=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $11}' | sort -n | head -n 1)

avg1=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $1 } END { print total/NR }')
avg2=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $2 } END { print total/NR }')
avg3=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $3 } END { print total/NR }')
avg4=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $4 } END { print total/NR }')
avg5=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $5 } END { print total/NR }')
avg6=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $6 } END { print total/NR }')
avg7=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $7 } END { print total/NR }')
avg8=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $8 } END { print total/NR }')
avg9=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $9 } END { print total/NR }')
avg11=$(cat /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $11 } END { print total/NR }')

pth=$(du -sh /home/$USER/ | awk '{print $2}')

mx="Max: ${max1},${max2},${max3},${max4},${max5},${max6},${max7},${max8},${max9},${pth},${max11}M"
mn="Min: ${min1},${min2},${min3},${min4},${min5},${min6},${min7},${min8},${min9},${pth},${min11}M"
avg="Avg: ${avg1},${avg2},${avg3},${avg4},${avg5},${avg6},${avg7},${avg8},${avg9},${pth},${avg11}M"

echo ${mx} >> /home/$USER/log/agg/metrics_agg_${tlg_wkt}.log
echo ${mn} >> /home/$USER/log/agg/metrics_agg_${tlg_wkt}.log
echo ${avg} >> /home/$USER/log/agg/metrics_agg_${tlg_wkt}.log

chmod 400 /home/$USER/log/tmp/metrics_agg_tmp_${tlg_wkt}.log
chmod 400 /home/$USER/log/agg/metrics_agg_${tlg_wkt}.log

# * */1 * * * /home/fathur45/Documents/VsCode/SISOP/Gitlab/soal-shift-sisop-modul-1-e05-2022/soal3/aggregate_minutes_to_hourly_log.sh